<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

//new test route. 
//NEED TO ADD: call to CheckinController to use Checkin()
Route::get('/checkin', 'CheckinController@Checkin');
// Route::get('/checkin', function(){


//     return 'CHECKIN';

// });
/*

MVC


Model/Database

View
(HTML, JSON)

Controller/Route
- Modelspp

*/

/*
$arrayVariable= $checkinController->CheckList()

return view('checklist', $arrayVariable);

*/

//additional test route. 
//NEED TO ADD: call to CheckinController to use ______ to display checkin info
Route::get('/checkinlist', 'CheckinController@CheckList');

Route::get('/checkinlist2','CheckinController@CheckList2');

//additional test route. 
//carrot tests
Route::get('/carrot/{colour}',[ 'uses'=>'carrotController@newCarrot']);
Route::get('/carrot', 'carrotController@Colours');