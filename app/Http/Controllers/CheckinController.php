<?php

namespace App\Http\Controllers;

use App\checkin;
use Illuminate\Http\Request;

use Illuminate\Support\Facades\DB;

class CheckinController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\checkin  $checkin
     * @return \Illuminate\Http\Response
     */
    public function show(checkin $checkin)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\checkin  $checkin
     * @return \Illuminate\Http\Response
     */
    public function edit(checkin $checkin)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\checkin  $checkin
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, checkin $checkin)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\checkin  $checkin
     * @return \Illuminate\Http\Response
     */
    public function destroy(checkin $checkin)
    {
        //
    }

    //create a checkin
    public function Checkin()
    {
        $checkin = new checkin;
        $didItWork='';
        $didItWork.= ($checkin->save());      
        return $didItWork;
       
    }

/*

    if ($checkinController->CheckList() == false) {

        //error handling
        echo "CAN't GET LIST";
    } 
    
*/

    //list checkins
    //Return an array of checkins
    public function CheckList()
    {
        $checkins= DB::table('checkins')->get();
        $count=1;
        echo 'Checkin List';
        echo '<br>';
       foreach ($checkins as $checkin){
           echo $count;
           echo '.  ';
            echo $checkin->created_at;
            echo '  ||  ';
            echo $checkin->updated_at;
            echo '  ||  ';
            if (!empty($checkin->deleted_at) ){
                echo $checkin->deleted_at;
            }
            echo  '<br> ';
            $count+=1;
        }
    }

    //return checkin array
     public function CheckList2()
    {
       $arrayVariable=checkin::all();

        return view('checkin', ['arrayVariable'=>$arrayVariable]);
    }

}
