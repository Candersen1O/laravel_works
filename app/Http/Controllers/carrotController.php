<?php

namespace App\Http\Controllers;

use App\carrot;
use App\Http\Controllers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class carrotController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\carrot  $carrot
     * @return \Illuminate\Http\Response
     */
    public function show(carrot $carrot)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\carrot  $carrot
     * @return \Illuminate\Http\Response
     */
    public function edit(carrot $carrot)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\carrot  $carrot
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, carrot $carrot)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\carrot  $carrot
     * @return \Illuminate\Http\Response
     */
    public function destroy(carrot $carrot)
    {
        //
    }

  /**
     * create new carrot with given colour
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\carrot  $carrot
     * @return \Illuminate\Http\Response
     */
    public function newCarrot($colour){
         $carrot = new carrot;
        $carrot->colour=$colour;
        $carrot->name='carrot';
        $carrot->qty=1;
        
        $carrot->save();
    }

      /**
     * view carrots in database
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\carrot  $carrot
     * @return \Illuminate\Http\Response
     */
    public function Colours(){
        $carrot= DB::table('carrot')->get();

       foreach ($carrot as $car){
            echo $car->colour;
            echo  '<br> ';
        }
       

        // $colours='';
        // @foreach ($carrot as $car)
        //     $colours.=$car->colour;
        // @endfor
        // var_dump($colours);
    }

}
